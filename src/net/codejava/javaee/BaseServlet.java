package net.codejava.javaee;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;

/**
 * Servlet implementation class BaseServlet
 */
@WebServlet("/BaseServlet")
public abstract class BaseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//	public static final String packageName = "com.camsbycbs.cams.servlet.";
	public static final String packageName = "net.codejava.javaee.";

	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		//		String yourName = request.getParameter("yourName");
		//		PrintWriter writer = response.getWriter();
		//		writer.println("<h1>Hello " + yourName + "</h1>");
		//		writer.close();
		@SuppressWarnings("rawtypes")
		Hashtable browserObj = new Hashtable();
		browserObj.put("req", req); browserObj.put("res", res);

		try {
			mainProcess(browserObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void mainProcess (@SuppressWarnings("rawtypes") Hashtable browserObj) throws Exception {
		HttpServletRequest req  = (HttpServletRequest)	browserObj.get("req");
		HttpServletResponse res = (HttpServletResponse)	browserObj.get("res");

		try{
			validateSession(browserObj);
		}catch(Exception e){
			System.out.println(e);
			HttpSession session = req.getSession();
			session.setAttribute("Exception",e);
			res.sendRedirect(packageName+"ExceptionDisplay");
		}
	}

	public void validateSession(@SuppressWarnings("rawtypes") Hashtable browserObj) throws Exception {
		HttpServletRequest req  = (HttpServletRequest) 	browserObj.get("req");
		HttpServletResponse res = (HttpServletResponse)	browserObj.get("res");

		int userId = 0;

		Cookie cookies[] = req.getCookies();

		if (cookies != null){
			for (int icookie = 0; icookie < cookies.length; icookie++){
				if (cookies[icookie].getName().equals("userId")){
					try{
						userId = Integer.parseInt(cookies[icookie].getValue());
					}catch(Exception e){
						System.out.println("Exception: Invalid Teller ID in cookie=[" + userId + "]");
					}
				}    
			}     
		}
//		HttpSession session = req.getSession();
		performOp(req, res, userId); 
	}

	protected void performOp(HttpServletRequest req, HttpServletResponse res,  int userId) throws Exception, IOException {
		res.setContentType("text/html");
		res.setBufferSize(1000);
		StringBuffer response = new StringBuffer(10000);


		response.append(performTask(req, userId));
		PrintWriter out = res.getWriter();
		out.print(response.toString());
		out.close();
		res.flushBuffer();
	}
	
    protected abstract String performTask(HttpServletRequest req, int userId) throws Exception;
}
